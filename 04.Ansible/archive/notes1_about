POSTGRES PROCESS AUTOMATION USING LINUX CONTAINERS AND ANSIBLE

About this presentation ...
- about automation
- about infrastructure/software/database as a service
- about linux containers (ubuntu)
- about ansible

============================================================
PROCESS AUTOMATION

A general technology term that is used to describe any process being automated through the use of computers and computer software. Processes that have been automated require less human intervention and less human time to deliver.

============================================================
INFRASTRUCTURE/SOFTWARE/DATABASE AS A SERVICE

Infrastructure As A Service
A form of cloud computing that provides virtualized computing resources over the Internet. IaaS is one of three main categories of cloud computing services, alongside Software as a Service (SaaS) and Platform as a Service (PaaS).

It is an online service that abstracts the user from the details of infrastructure like physical computing resources, location, data partitioning, scaling, security, backup etc.

Software As A Service
A software licensing and delivery model in which software is licensed on a subscription basis and is centrally hosted. It is sometimes referred to as "on-demand software". SaaS is typically accessed by users using a thin client via a web browser.

Database As A Service
A cloud computing service model that provides users with some form of access to a database without the need for setting up physical hardware, installing software or configuring for performance. Application owners do not have to install and maintain the database themselves. Instead, the database service provider takes responsibility for installing and maintaining the database, and application owners are charged according to their usage of the service.


REFERENCES:
  https://en.wikipedia.org/wiki/IAAS
  https://en.wikipedia.org/wiki/Software_as_a_service
  https://en.wikipedia.org/wiki/Cloud_database


============================================================
DATABASE AS A SERVICE

PostgreSQL Tasks (typical)
- provisioning
- replication
- tuning
- backups
- failover

============================================================
LINUX CONTAINERS
  https://linuxcontainers.org/
  https://en.wikipedia.org/wiki/LXC

LXC (Linux Containers) is an operating-system-level virtualization method for running multiple isolated Linux systems (containers) on a control host using a single Linux kernel.

============================================================
ANSIBLE
  https://www.ansible.com
  https://www.ansible.com/quick-start-video

A free-software platform for configuring and managing computers which combines multi-node software deployment, ad hoc task execution, and configuration management. It manages nodes over SSH or over PowerShell. Modules work over JSON and standard output and can be written in any programming language.

Typical Tasks:
- configuration management
- continuous delivery:  teams produce software in short cycles,
                        ensuring that the software can be reliably released at any time
- applications deployment
- security & compliance:
- provisioning
- orchestration:  aligning the business request with the applications, data, and infrastructure
                  by defining the policies and service levels through
                  automated workflows, provisioning, and change management

============================================================
ANSIBLE REFERENCES
  https://docs.ansible.com/intro_getting_started.html
  http://docs.ansible.com/ansible/intro_installation.html
  http://docs.ansible.com/ansible/intro_adhoc.html
  http://docs.ansible.com/ansible/modules.html
  http://docs.ansible.com/ansible/list_of_all_modules.html
  http://manpages.ubuntu.com/manpages/xenial/man1/ansible-doc.1.html
  https://www.mankier.com/1/ansible

