#!/bin/bash

set -e

p=$1
f=$2

rsync -av $1 /var/lib/postgresql/9.6/wal/$2

exit 0
